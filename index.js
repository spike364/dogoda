ymaps.ready().done(function (ym) {
    var myMap = new ym.Map(
        "YMaps",
        {
            center: [55.751574, 37.573856],
            zoom: 2,
            controls: []
        },
        {
            searchControlProvider: "yandex#search"
        }
    );
    myMap.behaviors.disable("scrollZoom");

    $("body").append("<button id='VP-CSP-14apr'>VP-CSP-14apr</button>");
    $("body").append("<button id='ER-ABE-14apr'>ER-ABE-14apr</button>");

    $("#VP-CSP-14apr").click(function () {
        myMap.geoObjects.removeAll();
        jQuery.getJSON("./VP-CSP-14apr.json", function (json) {
            json.forEach(el => {
                myMap.geoObjects.add(
                    new ymaps.Placemark([el.ac[0].lat, el.ac[0].lon, 17], {
                        balloonContentBody: JSON.stringify(el.ac[0])
                    })
                );
            });
        });
    });
    $("#ER-ABE-14apr").click(function () {
        myMap.geoObjects.removeAll();
        jQuery.getJSON("./ER-ABE-14apr.json", function (json) {
            json.forEach(el => {
                myMap.geoObjects.add(
                    new ymaps.Placemark([el.ac[0].lat, el.ac[0].lon, 17], {
                        balloonContentBody: JSON.stringify(el.ac[0])
                    })
                );
            });
        });
    });
});

// new ymaps.Placemark([59.869157, 30.3480523, 17]
