import React, { useState } from "react";
import { YMaps, Map, Placemark } from "react-yandex-maps";
import "./App.css";
import fligh1 from "./ER-ABE-14apr";
import fligh2 from "./VP-CSP-14apr";

const allFlights = [...fligh1, ...fligh2];
const destinations = [...new Set(allFlights.map(item => item.ac[0].reg))];

function App() {
    const [showingFlights, setShowingFlight] = useState([]);

    const handleButtonClick = e => {
        const dest = e.target.value;
        const selectedDests = allFlights.filter(
            flight => flight.ac[0].reg === dest
        );
        setShowingFlight(selectedDests);
    };

    return (
        <div className="App">
            <header className="App-header">
                <h1>Dogoda</h1>
                <div className="controls">
                    {destinations.map((dest, i) => (
                        <button
                            type="button"
                            value={dest}
                            key={i}
                            onClick={e => handleButtonClick(e)}
                        >
                            {dest}
                        </button>
                    ))}
                </div>
                <YMaps>
                    <div className="Map-wrapper">
                        <Map
                            defaultState={{
                                center: [55.751574, 37.573856],
                                zoom: 2,
                                controls: []
                            }}
                            width="100%"
                            height="60vh"
                        >
                            {showingFlights.map((flight, i) => (
                                <Placemark
                                    geometry={[
                                        flight.ac[0].lat,
                                        flight.ac[0].lon
                                    ]}
                                    key={i}
                                    modules={["geoObject.addon.balloon"]}
                                    properties={{
                                        balloonContentBody: JSON.stringify(
                                            flight.ac[0]
                                        )
                                    }}
                                />
                            ))}
                        </Map>
                    </div>
                </YMaps>
            </header>
        </div>
    );
}

export default App;
